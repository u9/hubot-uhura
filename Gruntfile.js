'use strict';

module.exports = function (grunt) {

  grunt.initConfig({
    mochaTest: {
      test: {
        options: {
          reporter: 'spec',
          require: 'coffee-script'
        },
        src: ['test/**/*.coffee']
      }
    },
    release: {
      options: {
        tagName: 'v<%= version %>',
        commitMessage: 'Prepared to release <%= version %>.'
      }
    },
    watch: {
      files: ['Gruntfile.js', 'src/**/*.coffee', 'test/**/*.coffee'],
      tasks: ['test']
    },
    coffeelint: {
      app: ['src/**/*.coffee'],
      options: {
        'arrow_spacing': {'level': 'warn'},
        'braces_spacing': {'level': 'warn'},
        'no_empty_functions': {'level': 'warn'},
        'no_empty_param_list': {'level': 'warn'},
        'no_interpolation_in_single_quotes': {'level': 'warn'},
        'no_this': {'level': 'warn'},
        'no_unnecessary_double_quotes': {'level': 'warn'},
        'space_operators': {'level': 'warn'},
        'spacing_after_comma': {'level': 'warn'}
      }
    }
  });

  // load all grunt tasks
  require('matchdep').filterDev(['grunt-*', '!grunt-cli']).forEach(grunt.loadNpmTasks);

  grunt.loadNpmTasks('grunt-coffeelint');

  grunt.registerTask('test', ['mochaTest']);
  grunt.registerTask('test:watch', ['watch']);
  grunt.registerTask('default', ['test']);
};
