# coffeelint: disable=max_line_length
#
# Description
#   A hubot script that talks to riker
#
# Configuration:
#   HUBOT_UHURA_RIKER_URL - URL pointing to an instance of Riker
#   HUBOT_UHURA_RIKER_TOKEN - Riker authorization token
#
# Commands:
#   hubot role <role> can deploy <project> - Give deploy permissions to a role
#   hubot role <role> can't deploy <project> - Remove deploy permissions from a role
#   hubot deploy <project> to <env> [version <version>] - Deploy a project to given <env>, optionally specifying a <version> to deploy
#
# Notes:
#
# Author:
#   Krzysztof Skoracki <krzysztof.skoracki@unit9.com>
#
# coffeelint: enable=max_line_length

config =
  riker_url: process.env.HUBOT_UHURA_RIKER_URL
  riker_token: process.env.HUBOT_UHURA_RIKER_TOKEN

urlEncode = (body) ->
  str = []

  for k, v of body
    str.push encodeURIComponent(k) + '=' + encodeURIComponent(v)

  return str.join '&'


module.exports = (robot) ->

  robot.respond /role ([^\s]+) can deploy ([^\s]+)/i, (res) ->
    isAdmin = robot.auth.hasRole(res.envelope.user, 'admin')

    unless isAdmin
      res.reply 'Sorry, only admins can assign deploy permissions.'
    else
      role = res.match[1].trim().toLowerCase()
      project = res.match[2].trim().toLowerCase()

      permissions = robot.brain.get('deploy_permissions') or {}

      permissions[role] or= []

      if project not in permissions[role]
        permissions[role].push project

      robot.brain.set('deploy_permissions', permissions)

      res.reply "#{role} can now deploy #{project}"

  robot.respond /role ([^\s]+) can't deploy ([^\s]+)/i, (res) ->
    isAdmin = robot.auth.hasRole(res.envelope.user, 'admin')

    unless isAdmin
      res.reply 'Sorry, only admins can assign deploy permissions.'
    else
      role = res.match[1].trim().toLowerCase()
      project = res.match[2].trim().toLowerCase()

      permissions = robot.brain.get('deploy_permissions') or {}

      if role of permissions
        permissions[role] = (x for x in permissions[role] when x isnt project)

        if permissions[role].length == 0
          delete permissions[role]

      robot.brain.set('deploy_permissions', permissions)

      res.reply "#{role} can't deploy #{project} anymore"

  robot.respond /deploy ([^\s]+) to ([^\s]+)(?:[\s,]*version ([^\s]+))?/i, (res) ->
    project = res.match[1].trim().toLowerCase()
    env = res.match[2].trim().toLowerCase()
    version = res.match[3].trim().toLowerCase() if res.match[3]

    roles = robot.auth.userRoles res.message.user

    permissions = robot.brain.get('deploy_permissions') or {}

    canDeploy = false

    for role in roles
      allowed_projects = permissions[role] or []
      if project in allowed_projects
        canDeploy = true
        break

    unless canDeploy
      res.reply "I'm afraid I can't do that"
    else
      if version
        res.reply "Deploying #{project} to #{env} (version #{version})"
      else
        res.reply "Deploying #{project} to #{env}"

      extraVars =
        env: env
        deploy_version: version

      # check if there are any extra arguments, e.g.
      # deploy project to prod extra tag1:value1 extra tag2:value2
      msg = res.message.text
      extras = msg.match(/extra\s([^\s]+)/ig)
      for extra in extras
        args = extra.slice(6)
        args = args.split(":")
        extraVars[args[0]] = args[1]

      data =
        user: res.message.user.name,
        command: 'deploy-' + project,
        extra_vars: JSON.stringify extraVars

      robot.http(config.riker_url)
        .header('Content-Type', 'application/x-www-form-urlencoded')
        .header('X-Access-Token', config.riker_token)
        .post(urlEncode(data)) (err, response, body) ->
          if err
            res.reply "Something's broken: #{err}"
