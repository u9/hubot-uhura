Helper = require('hubot-test-helper')
chai = require 'chai'

expect = chai.expect

helper = new Helper('../src/uhura.coffee')

describe 'uhura', ->
  beforeEach ->
    @room = helper.createRoom()

    roles = []

    @room.robot.auth =
      userRoles: (user) ->
        roles[user] or []

  afterEach ->
    @room.destroy()

  it 'doesn\'t let random people deploy', ->

    @room.user.say('alice', '@hubot deploy thing to dev').then =>
      expect(@room.messages).to.eql [
        ['alice', '@hubot deploy thing to dev']
        ['hubot', '@alice I\'m afraid I can\'t do that']
      ]

