# hubot-uhura

A hubot script that talks to [riker](https://bitbucket.org/u9/riker).
It allows admins to assign deployment permissions to particular roles, and users to perform the deployment.

## Installation

In hubot project repo, run:

`npm install hubot-uhura --save`

Then add **hubot-uhura** to your `external-scripts.json`:

```json
[
  "hubot-uhura"
]
```

## Configuration

The script supports the following environment variables:

- `HUBOT_UHURA_RIKER_URL` - URL pointing to an instance of Riker
- `HUBOT_UHURA_RIKER_TOKEN` - Riker authorization token

## Authorization

This script depends on [hubot-auth](https://github.com/hubot-scripts/hubot-auth)
(or anything that provides compatible API) for role management.

This script assumes the presence of the `robot.auth` object containing the following functions:

- `robot.auth.hasRole(user, role)` - returns `true` if user has a role, `false` otherwise
- `robot.auth.userRoles(user)` - returns an array of roles assigned to given user

Only people in the `admin` group can assign deployment permissions.

## Commands

This scripts provides the following commands:

- `hubot role <role> can deploy <project>` - Give deploy permissions to a role.
Only available to users who have the `admin` role.

- `hubot role <role> can't deploy <project>` - Remove deploy permissions from a role.
Only available to users who have the `admin` role.

- `hubot deploy <project> to <env> [version <version>]` - Deploy a project to given `<env>`, optionally specifying a `<version>` to deploy.
The user issuing this command needs to have a role that is allowed to deploy the particular project for the command to succeed.
If `<version>` is skipped, the default value configured in Riker will be used.

## Sample Interaction

```
user1>> hubot role wizard can deploy thing
hubot>> @user1 wizard can now deploy thing
user1>> hubot deploy thing to dev
hubot>> @user1 Deploying thing to dev
```

## NPM Module

https://www.npmjs.com/package/hubot-uhura

## Uhura?

Lieutenant Nyota Uhura, chief communication officer aboard the fictional
_USS Enterprise_ starship.
As a communication officer she is the appropriate person to pass the captain's
(your!) orders to commander Riker.

![Uhura](http://www.mallabo.net/wp-content/uploads/2014/06/Lt-Uhura2.png)
